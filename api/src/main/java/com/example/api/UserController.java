package com.example.api;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    private Long id;

    @Autowired
    UsersRepository usersRepository;

    public Long fetchIdWithEmail(String email) {
        this.id = usersRepository.fetchIdUsingEmail(email);
        return this.id;
    }

    @GetMapping(value = "/init")
    public ResponseEntity index() {
        return ResponseEntity.ok(usersRepository.findAll());
    }

    @PostMapping(value = "/create_user")
    public ResponseEntity createNewUser(@RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "password", required = true) String password,
            @RequestParam(value = "email", required = true) String email) {
        return ResponseEntity.ok(usersRepository.save(new Users(name, password, email)));
    }

    @GetMapping(value = "/{email}/get_user_info")
    public ResponseEntity getUserInfo(@PathVariable String email) {
        Optional<Users> foundUsersList = usersRepository.findById(fetchIdWithEmail(email));

        if (foundUsersList.isPresent()) {
            return ResponseEntity.ok(foundUsersList.get());
        } else {
            return ResponseEntity.badRequest().body("No Users found with id " + id + ". Please Check the id");
        }
    }

    @PutMapping(value = "/{email}/update_user_detail")
    public ResponseEntity updateBucketList(@PathVariable String email,
            @RequestParam(value = "name") Optional<String> name,
            @RequestParam(value = "password") Optional<String> password) {
        Optional<Users> optionalUserList = usersRepository.findById(fetchIdWithEmail(email));
        Long id = fetchIdWithEmail(email);
        
        if (!optionalUserList.isPresent()) {
            return ResponseEntity.badRequest().body("No Users found with id " + id + ". Please Check the id");
        }

        Users fetchingUserDetails = optionalUserList.get();

        if (name.isPresent()) {
            fetchingUserDetails.setName(name.get());
        }

        if (password.isPresent()) {
            fetchingUserDetails.setPassword(password.get());
        }
        
        return ResponseEntity.ok(usersRepository.save(fetchingUserDetails));
    }

    @DeleteMapping(value = "/{email}/delete_user")
    public ResponseEntity deleteUser(@PathVariable String email) {
        Long id = fetchIdWithEmail(email);
        
        usersRepository.deleteById(id);

        return ResponseEntity.ok().body("User Deleted.");
    }

}