package com.example.api;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class Users {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", length = 60, nullable = false)
    private String name;

    @Column(name = "password", length = 60, nullable = false)
    private String password;

    @Column(name = "email", unique = true, length = 60, nullable = false)
    private String email;

    Users() {}

    Users(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
    }

    Users(long id, String name, String password, String email) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
    }

    public void setId(long id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public long getId() { return this.id; }
    public String getName() { return this.name; }
    public String getPassword() { return this.password; }
    public String getEmail() { return this.email; }

    @Override
    public String toString() {
        return "UsersList{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}