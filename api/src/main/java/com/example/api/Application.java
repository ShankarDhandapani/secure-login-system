package com.example.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@SpringBootApplication
public class Application {

    @Autowired
    UsersRepository usersRepository;

    @RequestMapping("/")
    @ResponseBody
    String root() {
      return "Welcome to Secure Login System !!!";
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}